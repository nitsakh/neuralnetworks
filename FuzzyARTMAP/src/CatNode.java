import java.util.Arrays;
import java.util.Random;


public class CatNode {
	
	//Top down weights emanating from this node
	Double [] tdwts;
	//Inter-ART weights emanating from this node
	Double [] iawts;
	//Bottom-up Input
	Double bui=0d; 
	//Is it committed
	boolean isComm=false;
	//Count to maintain the number of times it was chosen and 
	//produced correct classification
	int chosencorrect = 0;
	//Count to maintain the number of times it was chosen
	int chosen = 0;
	//Ajk required for determining Confidence Factor
	Double ajk = 0d;
	//Sjk required for determining Confidence Factor
	Double sjk = 0d;
	//Confidence Factor
	Double CF=0d;
	
	//Constructors
	public CatNode(int nI,int nO){
		tdwts = new Double[nI];
		Arrays.fill(tdwts, 1.0);
		iawts = new Double[nO];
		Arrays.fill(iawts, 0.0);
	}
	
	public CatNode(CatNode c){
		this.tdwts = c.tdwts.clone();
		this.iawts = c.iawts.clone();
		this.bui = c.bui;
		this.isComm = c.isComm;
		this.chosen = c.chosen;
		this.chosencorrect = c.chosencorrect;
		this.sjk = c.sjk;
		this.ajk = c.ajk;
		this.CF = c.CF;
	}
	
	public void mutate(){
		Random rnd = new Random();
		int siz = tdwts.length/2;
		Double sdev = getStdDev();
		//Mutate lower corner of rect, i.e. u endpoint
		if(rnd.nextInt(1)==0){
			for(int i=0;i<siz;i++){
				Double mutval = rnd.nextGaussian()*sdev;
				tdwts[i]+=mutval;
				tdwts[i]=reset(tdwts[i]);
			}
		}
		//Mutate upper corner of rect, i.e. v endpoint
		else{
			for(int i=siz;i<tdwts.length;i++){
				Double mutval = rnd.nextGaussian()*sdev;
				tdwts[i]+=mutval;
				tdwts[i]=reset(tdwts[i]);
			}
		}
	}
	
	//Resets the value to 0 if it goes negative, and to 1 if it is greater
	private Double reset(Double val){
		if(val<0)
			return 0d;
		else if(val>1)
			return 1d;
		else
			return val;
	}
	
	//Returns the pre decided value of standard deviation
	private Double getStdDev(){
		return 0.05*(1-CF);
	}
}
