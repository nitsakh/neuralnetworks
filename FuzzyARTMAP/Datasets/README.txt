The dataset contains instances for training, testing and validation.
Each folder is a separate dataset.
Each file in the dataset consists of different instances 
separated by newlines.
For each instance, the inputs and the outputs are separated by a comma (,).
Each attribute value in the inputs and outputs is separated using a
single space.