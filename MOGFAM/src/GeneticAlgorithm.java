import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;


public class GeneticAlgorithm {
	
	int POP_SIZE = 20;
	int MAX_GEN = 500;
	Double mutRate = 0.05;
	public static String dataset = "Iris5000";
	
	ArrayList<Chromosome> population = new ArrayList<Chromosome>();
	ArrayList<Chromosome> archive = new ArrayList<Chromosome>();
	ArrayList<Chromosome> ap = new ArrayList<Chromosome>();
	ArrayList<Chromosome> temppop = new ArrayList<Chromosome>();
	Random rnd = new Random();
	int gen = 0;
	
	//Constructor
	public GeneticAlgorithm(){
		
	}
	
	public void startGA() throws IOException{
		createPopulation();
		
		while(gen<MAX_GEN){
			
			//Evaluate population
			for(Chromosome ch:population){
				ch.evaluate();
			}
			temppop = null;
			temppop = new ArrayList<Chromosome>();
			ap = new ArrayList<Chromosome>();
			setArchive();
			System.out.println("\n################## ARCHIVE ###################");
			for(Chromosome ch:archive){
				System.out.println("Generation: "+gen);
				System.out.println("Complexity: "+ch.complexity);
				System.out.println("Accuracy: "+(1-ch.error));
				System.out.println("\n-------------------------------\n");
			}
			ap.addAll(population);
			ap.addAll(archive);
			assignFitness();
			
			//Creating a temp population after selection
			Chromosome lc = new Chromosome(leastComplex());
			temppop.add(lc);
			Chromosome le = new Chromosome(leastError());
			temppop.add(le);
			while(temppop.size()<POP_SIZE){
				
				temppop.add(new Chromosome(select()));
			}
			population = null;
			population = new ArrayList<Chromosome>();
			//Creating a temp population after crossover
			for(int i=0;population.size()<POP_SIZE-1;i++){
				Chromosome c1 = temppop.get(i);
				Chromosome c2 = temppop.get(i+1);
				crossover(c1, c2);
				population.add(new Chromosome(c1));
				population.add((c2));
			}
			
			//Calculate confidence factors for all in population
			for(Chromosome ch:population){
				calcCF(ch);
			}
			
			//Create a population after mutation
			temppop = null;
			temppop = new ArrayList<Chromosome>();
			for(Chromosome ch:population){
				mutate(ch);
				temppop.add(ch);
			}
			
			//Create final population after pruning categories
			population = null;
			population = new ArrayList<Chromosome>();
			for(Chromosome ch:temppop){
				prune(ch);
				population.add(ch);
			}
			gen++;
		}
	}
	
	//Calculate Confidence Factor
	public void calcCF(Chromosome ch){
		ArrayList<CatNode> tempgene = new ArrayList<CatNode>();
		for(CatNode c:ch.genes){
			tempgene.add(c);
		}
		while(!tempgene.isEmpty()){
			CatNode cn1 = tempgene.get(0);
			ArrayList<CatNode> temp= new ArrayList<CatNode>();
			temp.add(cn1);
			int i=0;
			while(i<tempgene.size()){
				CatNode cn2 = tempgene.get(i++);
				if(Arrays.equals(cn1.iawts,cn2.iawts)){
					temp.add(cn2);
					tempgene.remove(cn2);
					i--;
				}
				else
					break;
			}
			
			//Determine Best category accuracy from temp
			int max = -999;
			for(CatNode cn:temp){
				if(cn.chosencorrect>max){
					max = cn.chosencorrect;
				}
			}
			
			//Assign Ajk values for each category
			for(CatNode cn:temp){
				cn.ajk = cn.chosencorrect*1d/max;
			}
			
			//Determine Most selected category from temp
			int maxchosen = -999;
			for(CatNode cn:temp){
				if(cn.chosen>max){
					maxchosen = cn.chosen;
				}
			}
			
			//Assign Ajk values for each category
			for(CatNode cn:temp){
				cn.ajk = cn.chosen*1d/maxchosen;
			}
			
			//Calculate confidence factor according to equation
			for(CatNode cn:temp){
				cn.CF = 0.5*cn.ajk+0.5*cn.sjk;
			}
			
			tempgene.remove(cn1);
		}
	}
	
	//Crossover Operator
	public void crossover(Chromosome p1, Chromosome p2) throws IOException{
		ArrayList<CatGrp> cgrp1 = p1.ggrp;
		ArrayList<CatGrp> cgrp2 = p2.ggrp;
		for(int i=0;i<cgrp1.size();i++){
			cgrp1.get(i).crossover(cgrp2.get(i));
		}
		p1.evaluate();
		p2.evaluate();
	}
	
	
	
	
	
	//Mutation Operator
	public void mutate(Chromosome ch){
		for(CatNode c:ch.genes){
			c.mutate();
		}
		ch.complexity = ch.genes.size();
	}
	
	//Prune Operator
	public void prune(Chromosome ch){
		for(CatGrp cg:ch.ggrp){
			cg.prune(ch);
		}
		ch.complexity = ch.genes.size();
	}
	
	//Function for deterministic binary tournament selection
	public Chromosome select() throws IOException{
		Chromosome t1 = population.get(rnd.nextInt(POP_SIZE));
		Chromosome t2 = population.get(rnd.nextInt(POP_SIZE));
		
		if(t1.fitness<t2.fitness){
			return new Chromosome(t1);
		}
		else
			return new Chromosome(t2);
		
	}
	
	//Function which calculates strength of each chromosome, and then
	//Raw fitness is assigned, which is then adjusted
	public void assignFitness(){
		
		//Calculation of strength of all individuals
		for(Chromosome c:population)
			calculateStrength(c);
		
		//Calculates raw fitness
		for(Chromosome c:population)
			calcRawFitness(c);
		
		//Calculate k as sqrt of sum of size of A + P
		Double k =Math.sqrt(archive.size()+POP_SIZE);
		//Calculate fitness
		for(Chromosome c:population)
			calcFitness(c,k);
	}
	
	//Function to adjust raw fitness to calculate actual fitness
	public void calcFitness(Chromosome ch, Double k){
		
		//Determine the distance of all members from current node
		ArrayList<Double> dist = new ArrayList<Double>();
		for(Chromosome o:population){
			dist.add(ch.getDistance(o));
		}
		
		Collections.sort(dist);
		Double sigmak = dist.get(k.intValue());
		Double temp = 1.0/(sigmak+2);
		ch.setFitness(ch.getRx()+temp);
	}
	
	//Function which calculates the sum of strengths of all dominators
	public void calcRawFitness(Chromosome ch){
		int sum = 0;
		for(int d = 0;d<population.size();d++){
			Chromosome temp = population.get(d);
			if(ch == temp){
				continue;
			}
			if(temp.doesDominate(ch))
				sum+=population.get(d).strength;
		}
		ch.setRx(sum);
	}
	
	//Function which calculates strength of chromosomes
	public void calculateStrength(Chromosome ch){
		int strength = 0;
		for(int d = 0;d<population.size();d++){
			Chromosome temp = population.get(d);
			if(ch == temp){
				continue;
			}
			if(ch.doesDominate(temp))
				strength++;
		}
		ch.setStrength(strength);
	}
	
	//Function to Manage members in Archive
	public void setArchive() throws IOException{
		if(gen == 0)
			for(Chromosome ch:population)
				archive.add(new Chromosome(ch));
		else{
			ArrayList<Chromosome> nondominated = new ArrayList<Chromosome>();
			int flag = 0;
			
			//Find solutions in P that are nondominated by those in A
			for(int i=0;i<population.size();i++){
				Chromosome p = population.get(i);
				for(int j=0;j<archive.size();j++){
					Chromosome a = archive.get(j);
					if(p.doesDominate(a)){
						flag = 1;
					}
					else{
						flag = 0;
						break;
					}
				}
				if(flag == 1){
					nondominated.add(new Chromosome(p));
				}
			}
			
			//Find solutions in A that are now dominated by list "nondominated"
			for(int i=0;i<nondominated.size();i++){
				Chromosome z = nondominated.get(i);
				for(int j=0;j<archive.size();j++){
					Chromosome a = archive.get(j);
					if(z.doesDominate(a)){
						archive.remove(a);
					}
				}
			}
			
			//Add these new solutions to archive
			archive.addAll(nondominated);
		}
	}
	
	//Function to create initial population for the GA
	//It provide random values for rho_a_bar between 0.1 and 0.95
	//and random list presentation sequence
	public void createPopulation() throws IOException{
		ArrayList<Double []> input=new ArrayList<Double[]>();
		ArrayList<Double []> output=new ArrayList<Double[]>();
		
		for(int i=0;i<POP_SIZE;i++){
			//0 - Training, 1 - Validation, 2 - Testing
			getSetData(input, output,0);
			Double choice = 0.01;
			//Get random baseline vigilance parameter value between 0.1 and 0.95
			Double bVigilance = rnd.nextDouble()*(0.95-0.1)+0.1;
			FAM f = new FAM(bVigilance,choice);
			f.Train(input, output, 100);
			Chromosome c = new Chromosome(f.getCatNodes());
			population.add(c);
		}
	}
	
	public void getSetData(ArrayList<Double []> in, ArrayList<Double []> out, int type) throws IOException{
		DataManager dm =new DataManager(in, out);
		dm.getData(dataset, type);
	}
	
	//Function to find node with least complexity
	public Chromosome leastComplex(){
		int min = 999,index = 0;
		for(int i=0;i<POP_SIZE;i++){
			if(population.get(i).complexity<min && population.get(i).complexity>0){
				min = population.get(i).complexity;
				index = i;
			}
		}
		return population.get(index);
	}
	
	//Function to find node with least error
	public Chromosome leastError(){
		Double min = 999.0;
		int index = 0;
		for(int i=0;i<POP_SIZE;i++){
			if(population.get(i).error<min){
				min = population.get(i).error;
				index = i;
			}
		}
		return population.get(index);
	}
}
