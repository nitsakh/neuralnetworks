import java.io.IOException;


public class MainClass {
	
	public static void main(String[] args) throws IOException{
		GeneticAlgorithm ga = new GeneticAlgorithm();
		ga.startGA();
		System.out.println();
	}
}
